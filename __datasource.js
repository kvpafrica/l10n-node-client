/**
 * Created by nonami on 03/12/2019.
 */


const request = require('request-promise');
const redis = require('redis');
const config = require('./__config');


const KEY_PREFIX = 'l10n';
const redisClient = redis.createClient(config.redis);
const {promisify} = require('util');
const cacheFetch = promisify(redisClient.get).bind(redisClient);
const cachePut = promisify(redisClient.set).bind(redisClient);

// Base class for localization source
class LocalizationDataSource {

  buildCacheKey(type, country, lang, mKey) {
    let k = `${KEY_PREFIX}-${type}-${country.toLowerCase()}`;
    if (lang) {
      k += `-${lang.toLowerCase()}`;
    }
    if (mKey) {
      k += `-${mKey.toLowerCase()}`;
    }
    return k;
  }

}


// Cache source class
class CacheSource extends LocalizationDataSource{

  async getCountryConfig(country) {
    console.log(this.buildCacheKey('config', country));
    const value = await cacheFetch(this.buildCacheKey('config', country));
    if (!value) {
      return null;
    }
    return JSON.parse(value);
  }

  async getPushMessage(country, lang, key) {
    const value = await cacheFetch(this.buildCacheKey('push', country, lang, key));
    if (!value) {
      return null;
    }
    return JSON.parse(value);
  }

  getSmsMessage(country, lang, key) {
    return cacheFetch(this.buildCacheKey('sms', country, lang, key));
  }

  getErrorMessage(country, lang, key) {
    return cacheFetch(this.buildCacheKey('error', country, lang, key));
  }

  getText(country, lang, key) {
    return cacheFetch(this.buildCacheKey('text', country, lang, key));
  }

}


// Service source class (Fetches from l10n-service and saves in cache
class ServiceSource extends LocalizationDataSource{

  __request(path, country, lang) {
    const url = `${config.service.url}${path}`;
    const headers = {};
    if (country) {
      headers['X-Entity'] = country.toUpperCase();
    }

    if (lang) {
      headers['X-Lang'] = lang.toUpperCase()
    }
    return request({
      url,
      headers,
      method: 'GET',
      json: true
    }).catch(err => {
      console.error(err);
      return null;
    })
  }

  __cache(type, country, lang, key, data) {
    return cachePut(this.buildCacheKey(type, country, lang, key), data, 'EX', config.cacheTtl)
      .catch(err => {
        console.error(err);
        return false;
      })
  }

  async getCountryConfig(country) {
    const config = await this.__request('/config', country);
    if (config) {
      await this.__cache('config', country, null, null, JSON.stringify(config));
    }
    return config;
  }

  async getPushMessage(country, lang, key) {
    const path = `/translations/push/${key}`;
    const msg = await this.__request(path, country, lang);
    if (msg) {
      await this.__cache('push', country, lang, key, JSON.stringify(msg));
    }
    return msg;
  }

  async getSmsMessage(country, lang, key) {
    const path = `/translations/sms/${key}`;
    const msg = await this.__request(path, country, lang);
    if (msg) {
      await this.__cache('sms', country, lang, key, msg);
    }
    return msg;
  }

  async getErrorMessage(country, lang, key) {
    const path = `/translations/error/${key}`;
    const msg = await this.__request(path, country, lang);
    if (msg) {
      await this.__cache('error', country, lang, key, msg);
    }
    return msg;
  }

  async getText(country, lang, key) {
    const path = `/translations/text/${key}`;
    const msg = await this.__request(path, country, lang);
    if (msg) {
      await this.__cache('text', country, lang, key, msg);
    }
    return msg;
  }

}



module.exports.cacheSource = new CacheSource();
module.exports.serviceSource = new ServiceSource();
module.exports.localizationDataSource = new LocalizationDataSource();