/**
 * Created by nonami on 03/12/2019.
 */
module.exports = {
  redis: {
    host: process.env.LOCALIZATION_REDIS_HOST || 'staging-lock-server.0rvxze.0001.usw2.cache.amazonaws.com',
    port: process.env.LOCALIZATION_REDIS_PORT || '6379'
  },
  service: {
    url: process.env.LOCALIZATION_SERVICE_URL || 'https://6snxs5gms7.execute-api.us-west-2.amazonaws.com/staging'
  },
  cacheTtl: parseInt(process.env.LOCALIZATION_CACHE_TTL_SECONDS) || 7200
};