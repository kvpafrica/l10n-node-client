/**
 * Created by Demilade on 07/01/2020.
 */


const chai = require("chai");
const expect = chai.expect;
const rewire = require("rewire");
const index = rewire('../index');

const processTemplate = index.__get__("processTemplate");

describe('Index Test', function(){

    describe('processTemplate Method', function(){

        it('Should return "My name is Demilade" for argument ("My name is {{firstname}}", {firstname: "Demilade"})', function () {
            expect(processTemplate("My name is {{firstname}}", {firstname: 'Demilade'})).to.equal("My name is Demilade");
        })
    })
});