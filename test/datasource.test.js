/**
 * Created by Demilade on 06/01/2020.
 */


const chai = require("chai");
const expect = chai.expect;
const {localizationDataSource, cacheSource, serviceSource} = require('../__datasource');

describe('__datasource Test', function(){

    describe('LocalizationDataSource Class', function(){

        it('Should contain method buildCacheKey', function () {
            expect(localizationDataSource).to.exist;
            expect(localizationDataSource.buildCacheKey).exist;
        })

        describe('BuildCacheKey method Test', function () {
            it('Should return "l10n-push-ng" when called with argument ("push", "ng")', function () {
                expect(localizationDataSource.buildCacheKey('push', 'ng')).to.equal('l10n-push-ng');
                expect(localizationDataSource.buildCacheKey('push', 'Ng')).to.equal('l10n-push-ng');
                expect(localizationDataSource.buildCacheKey('push', 'nG')).to.equal('l10n-push-ng');
            } )

            it('Should return "l10n-push-ng-en" when called with argument ("push", "NG", "EN")', function () {
                expect(localizationDataSource.buildCacheKey('push', 'NG', 'EN')).to.equal('l10n-push-ng-en');
                expect(localizationDataSource.buildCacheKey('push', 'nG', 'eN')).to.equal('l10n-push-ng-en');
            } )

            it('Should return "l10n-push-ng-en-my_key" when called with argument ("push", "NG", "EN", "MY_KEY")', function () {
                expect(localizationDataSource.buildCacheKey('push', 'NG', 'EN', "MY_KEY")).to.equal('l10n-push-ng-en-my_key');
                expect(localizationDataSource.buildCacheKey('push', 'Ng', 'eN', "MY_Key")).to.equal('l10n-push-ng-en-my_key');
            } )
        })
    })

    describe('CacheSource Test', function () {
        it('Contains buildCacheKey extended from LocalizationDataSource', function () {
            expect(cacheSource).exist;
            expect(cacheSource.buildCacheKey).exist;
        })

        describe('getCountryConfig Test', function () {
            it('exists', function () {
                expect(cacheSource.getCountryConfig).exist;
            })
        })
        describe('getPushMessage Test', function () {
            it('exists', function () {
                expect(cacheSource.getPushMessage).exist;
            })
        })
        describe('getSMSMessage Test', function () {
            it('exists', function () {
                expect(cacheSource.getSmsMessage).exist;
            })
        })

        describe('getErrorMessage Test', function () {
            it('exists', function () {
                expect(cacheSource.getErrorMessage).exist;
            })
        })

        describe('getTextMessage Test', function () {
            it('exists', function () {
                expect(cacheSource.getText).exist;
            })
        })
    })

    describe('ServiceSource Test', function () {
        it('Contains buildCacheKey extended from LocalizationDataSource', function () {
            expect(serviceSource).exist;
            expect(serviceSource.buildCacheKey).exist;
        })

        describe('getCountryConfig Test', function () {
            it('exists', function () {
                expect(serviceSource.getCountryConfig).exist;
            })
        })
        describe('getPushMessage Test', function () {
            it('exists', function () {
                expect(serviceSource.getPushMessage).exist;
            })
        })
        describe('getSMSMessage Test', function () {
            it('exists', function () {
                expect(serviceSource.getSmsMessage).exist;
            })
        })

        describe('getErrorMessage Test', function () {
            it('exists', function () {
                expect(serviceSource.getErrorMessage).exist;
            })
        })

        describe('getText Test', function () {
            it('exists', function () {
                expect(serviceSource.getText).exist;
            })
        })
    })
})