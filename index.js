/**
 * Created by nonami on 02/12/2019.
 */

const {cacheSource, serviceSource} = require('./__datasource');
const Handlebars = require('handlebars');

const delegate = async (method, ...args) => {
  let result = await cacheSource[method](...args);
  if (!result) {
    result = await serviceSource[method](...args);
  }
  return result;
};

/**
 * returns a string generated from a template
 *  e.g template=my name is {{firtname}} with vars={firstname: Demilade} becomes my name is Demilade.
 * @param template - String
 * @param {JSON} vars - object containing key and value for the template varaiables
 * @returns {String}
 */
const processTemplate = (template, vars) => {
    try{
      return Handlebars.compile(template)(vars);
    }catch (e) {
      console.log('HANDLEBARS_COMPILE_ERROR for', template, vars, e.message);
      return '';
    }
};

exports.getConfig = (country) => {
  return delegate('getCountryConfig', country);
};

exports.getPushMessage = (country, lang, key) => {
  return delegate('getPushMessage', country, lang, key);
};


exports.getSmsMessage = (country, lang, key) => {
  return delegate('getSmsMessage', country, lang, key);
};

exports.getError = async (country, lang, key, defaultTo = null) => {
  const errorMessage = await delegate('getErrorMessage', country, lang, key);
  return errorMessage || defaultTo;
};

/**
 *
 * @param country
 * @param lang - language
 * @param key - key of the message to be gotten from l10n-assets
 * @param {JSON} templateVars - object containing key and value for the template varaiables
 * @param defaultTo
 * @returns {Promise<*>}
 */
exports.getText = async (country, lang, key, templateVars, defaultTo = null) => {
  let textMessage = await delegate('getText', country, lang, key);
  textMessage = processTemplate(textMessage, templateVars);
  return textMessage || defaultTo;
};



